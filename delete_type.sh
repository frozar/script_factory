#!/bin/bash

usage(){
    # display the number of argument given
    # echo $#
    echo "  Usage:" $0 "<file_name> <type_name>"
}

if [ $# -ne 2 ]
then
    usage 
    exit -1
fi

f=$1
typ=$2

f_tmp=/tmp/${f}_tmp

beg=`grep -ni -e "type *${typ}_" -e "^ *end *type" $f | grep -i -e "^[0-9]*: *!\? *type *$typ" -A1 | head -n1 | cut -d":" -f1`
end=`grep -ni -e "type *${typ}_" -e "^ *end *type" $f | grep -i -e "^[0-9]*: *!\? *type *$typ" -A1 | tail -n1 | cut -d":" -f1`

if [ "${beg}" == "" ]
then
    exit
fi

if [ "${end}" == "" ]
then
    exit
fi

head -n $(( ${beg}-1 )) $f > $f_tmp
nb_line=$( wc -l $f | cut -d" " -f1 )
tail -n $(( ${nb_line} - ${end} )) $f >> $f_tmp

mv $f_tmp $f

echo "INFO: delete type $typ in $f"
