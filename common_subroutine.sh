#!/bin/bash

files=( $(ls mod_[A-Z][A-Z][A-Z][A-Z][a-z0-9].f90) )

first_file=${files[0]}
# echo $first_file

subroutines=$(to_del=$(echo $first_file | sed "s/mod//g;s/.f90//g"); grep -i "subroutine" mod${to_del}.f90 | grep -vi -e "end subroutine" -e "\!\!" -e " *!" | grep -i subroutine | sed "s/ *subroutine //g;s/ *SUBROUTINE //g;s/(.*//g" | sort | sed "s/${to_del}//g")

# grep -i "subroutine *CLALp2ENT" mod_CLJCx.f90
# grep -i "subroutine *clean_memory" mod_CLJCx.f90

i=0

# for s in CLALp2ENT clean_memory
for s in ${subroutines[*]}
do
    # echo " s:" $s
    in_all="true"

    # for f in mod_CLALp.f90 mod_CLJCx.f90
    for f in ${files[*]}
    do 
	# echo " the test"
	# echo $(grep -i "subroutine *${s}" ${f})
	if [ "`grep -i "subroutine *${s}" ${f}`" == "" ]
	then
	    # echo " f:" $f
	    # grep -i "subroutine *${s}" ${f}
	    in_all="false"
	fi
    done

    # echo " in_all:" $in_all
    # echo ""

    if [ $in_all == "true" ]
    then
	elected_subroutines[${i}]=$s
	i=$((${i} + 1))
    fi

done

# echo ${elected_subroutines[*]}

for i in ${elected_subroutines[*]}
do
    echo $i
done

echo "Array length:" ${#elected_subroutines[*]}
