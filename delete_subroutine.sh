#!/bin/bash

usage(){
    # display the number of argument given
    # echo $#
    echo "  Usage:" $0 "<file_name> <subroutine_name>"
}


if [ $# -ne 2 ]
then
    usage 
    exit -1
fi

f=$1
sub=$2

f_tmp=/tmp/${f}_tmp

beg=`grep -ni -e "subroutine *${sub}" -e "end *subroutine" $f | grep -i -e "^[0-9]*: *!\? *subroutine *$sub" -A1 | head -n1 | cut -d":" -f1`
end=`grep -ni -e "subroutine *${sub}" -e "end *subroutine" $f | grep -i -e "^[0-9]*: *!\? *subroutine *$sub" -A1 | tail -n1 | cut -d":" -f1`

if [ "${beg}" == "" ]
then
    exit
fi

if [ "${end}" == "" ]
then
    exit
fi

head -n $(( ${beg}-1 )) $f > $f_tmp
nb_line=$( wc -l $f | cut -d" " -f1 )
tail -n $(( ${nb_line} - ${end} )) $f >> $f_tmp

mv $f_tmp $f

echo "INFO: delete subroutine '${sub}()' in $f"
