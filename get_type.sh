#!/bin/bash

usage(){
    # display the number of argument given
    # echo $#
    echo "  Usage:" $0 "<subroutine_name>"
}

files=( $(ls mod_[A-Z][A-Z][A-Z][A-Z][a-z0-9].f90) )

if [ $# -ne 1 ]
then
    usage 
    exit -1
fi

typ=$1
outfile=l_t_${typ}

>$outfile
(
for f in ${files[*]}
do 
    echo "  ====== $f" 
    beg=`grep -ni -e "^ *type *${typ}_" -e "^ *end *type" $f | grep -i -e "^[0-9]*: *type *$typ" -A1 | head -n1 | cut -d":" -f1`
    end=`grep -ni -e "^ *type *${typ}_" -e "^ *end *type" $f | grep -i -e "^[0-9]*: *type *$typ" -A1 | tail -n1 | cut -d":" -f1`
    head -n $end $f | tail -n $((${end}-${beg}+1))
    echo "  ====== END $f"
    echo ""
done
) > $outfile

echo "INFO: $f produced"
