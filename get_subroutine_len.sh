#!/bin/bash

usage(){
    # display the number of argument given
    # echo $#
    echo "  Usage:" $0 "<file_name>"
}

if [ $# -ne 1 ]
then
    usage 
    exit -1
fi

f=$1

sub_names=`grep -i subroutine $f | grep -iv -e " *\!.*" -e " *end *subroutine" | grep -i subroutine | python -c "import sys; l = map(lambda x: x.split(), sys.stdin.readlines()); print '\n'.join(map(lambda x : x[1], l ))" | cut -d"(" -f1`

( for sub in ${sub_names}
do
    beg=`grep -ni "subroutine" $f | grep -i "subroutine *${sub}(\?" -A1 | head -n1 | cut -d":" -f1`
    end=`grep -ni "subroutine" $f | grep -i "subroutine *${sub}(\?" -A1 | head -n2 | tail -n1 | cut -d":" -f1`
    echo -e "$(( $end - $beg + 1 ))\t: $sub"
done ) | sort -rn
