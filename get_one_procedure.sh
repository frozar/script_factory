#!/bin/bash

usage(){
    # display the number of argument given
    # echo $#
    echo "  Usage:" $0 "<file_name> <subroutine_name>"
    echo "  Example:" $0 "mod_DKDKx.f90 get_antac"
}

if [ $# -ne 2 ]
then
    usage 
    exit -1
fi

f=$1
sub=$2

f_id=$(basename $f)
id=$(echo ${f_id:4:4})

beg=`grep -ni -e "^ *\(subroutine\|.*function\) *\(${sub}\(_\|.\|\)${id}\|${id}\(_\|.\|\)${sub}\)" -e "^ *end *\(subroutine\|function\)" $f | grep -i -e "^[0-9]*: *\(subroutine\|.*function\) *\(${sub}\(_\|.\|\)${id}\|${id}\(_\|.\|\)${sub}\).*(" -A1 | head -n2 | head -n1 | cut -d":" -f1`
end=`grep -ni -e "^ *\(subroutine\|.*function\) *\(${sub}\(_\|.\|\)${id}\|${id}\(_\|.\|\)${sub}\)" -e "^ *end *\(subroutine\|function\)" $f | grep -i -e "^[0-9]*: *\(subroutine\|.*function\) *\(${sub}\(_\|.\|\)${id}\|${id}\(_\|.\|\)${sub}\).*(" -A1 | head -n2 | tail -n1 | cut -d":" -f1`

head -n $end $f | tail -n $((${end}-${beg}+1))
