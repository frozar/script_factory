#!/bin/bash

usage(){
    # display the number of argument given
    # echo $#
    echo "  Usage:" $0 "<subroutine_name>"
}

files=( $(ls mod_[A-Z][A-Z][A-Z][A-Z][a-z0-9].f90) )

if [ $# -ne 1 ]
then
    usage 
    exit -1
fi

sub=$1
outfile=l_s_${sub}

>$outfile
(
for f in ${files[*]}
do 
    echo "  ====== $f" 
    id=$(echo ${f:4:5})
    beg=`grep -ni -e "^ *subroutine *${sub}_${id}" -e "^ *end *subroutine" $f | grep -i -e "^[0-9]*: *subroutine *$sub" -A1 | head -n2 | head -n1 | cut -d":" -f1`
    end=`grep -ni -e "^ *subroutine *${sub}_${id}" -e "^ *end *subroutine" $f | grep -i -e "^[0-9]*: *subroutine *$sub" -A1 | head -n2 | tail -n1 | cut -d":" -f1`
    head -n $end $f | tail -n $((${end}-${beg}+1))
    echo "  ====== END $f"
    echo ""
done
) > $outfile

echo "INFO: $outfile produced"
