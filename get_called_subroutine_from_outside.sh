#!/bin/bash

usage(){
    # display the number of argument given
    # echo $#
    echo "  Usage:" $0 "<src_file> <root_dir> <exclude_dir>"
}

if [ $# -ne 3 ]
then
    usage 
    exit -1
fi

src_file=$1
root_dir=$2
exclude_dir=$3

l_sub=$( grep -i "subroutine" ${src_file} | grep -vi "end *subr" | sed "s/ *subroutine *//g;s/(.*//g" | grep -v "^!" )

for s in ${l_sub}
do 
    cmd=`grep -rni "call *${s}" ${root_dir} | grep -v ${exclude_dir}`
    if [ "${cmd}" != '' ]
    then 
	echo "${s}"
    fi
done

